export default class BaseTour {
    constructor() {
        this.tour = introJs();

        let me = this;
        this.tour.onbeforechange(function(targetElement) {
            let currentStep = me.tour._currentStep;
            console.log(currentStep);
            me._onStepChange(currentStep, targetElement);
        });

        let steps = this._getTourSteps();
        this._configureTour();
        this._configureSteps(steps);
    }

    /* -------------------------------------------- */

    /**
     * @typedef {Object} TourStep
     * @param {string|null} element
     * @param {string} intro
     * @param {string|null| position}
     */

    /* -------------------------------------------- */

    /**
     * Implement this method in order to configure the steps of the Tour
     * @abstract
     * @returns {TourStep[]}
     */
    _getTourSteps() {
        throw new Error("_getTourSteps must be implemented by a subclass");
    }

    /* -------------------------------------------- */

    /**
     *
     * @protected
     */
    _configureTour() {
        this.tour.setOption('tooltipPosition', 'auto');
        this.tour.setOption('positionPrecedence', ['right', 'left', 'top', 'bottom']);
        this.tour.setOption('showProgress', true);
    }

    /* -------------------------------------------- */

    /**
     * @param {TourStep[]} steps
     * @protected
     */
    _configureSteps(steps) {
        this.tour.setOptions({
            steps: steps.map(s => { return { element: document.querySelector(s.element), intro: game.i18n.localize(s.intro), position: s.position }})
        });
    }

    /* -------------------------------------------- */

    /**
     * Invoked just before a Step is about to be shown
     * @param {number} currentStep The step of the tour about to be shown (0 indexed)
     * @param {Element|undefined} targetElement The targeted element for the tooltip, if any
     * @protected
     */
    _onStepChange(currentStep, targetElement) {

    }

    /* -------------------------------------------- */

    /**
     * Starts the Tour
     */
    start() {
        this.tour.start();
    }

    /* -------------------------------------------- */

    /**
     * Awaits for the specified amount of milliseconds before returning
     * @param ms
     * @return {Promise}
     * @public
     */
    static delay(ms) {
        return new Promise(res => setTimeout(res, ms));
    }
}
