import BaseTour from "./BaseTour.mjs";

export default class SidebarTour extends BaseTour {
    _onStepChange(currentStep, targetElement) {
        let $target = $(targetElement);
        if ($target.hasClass("fas")) {
            $target.click();
        }
    }

    /* -------------------------------------------- */

    _getTourSteps() {
        return [
            { // 0
                element: '#sidebar-tabs',
                intro: "TOUR.Sidebar.0"
            },
            { // 1
                element: '.fa-comments',
                intro: "TOUR.Sidebar.1"
            },
            { // 2
                element: '.fa-fist-raised',
                intro: "TOUR.Sidebar.2"
            },
            { // 3
                element: '.fa-map',
                intro: "TOUR.Sidebar.3"
            },
            { // 4
                element: '.fa-users',
                intro: "TOUR.Sidebar.4"
            },
            { // 5
                element: '.fa-suitcase',
                intro: "TOUR.Sidebar.5"
            },
            { // 6
                element: '.fa-book-open',
                intro: "TOUR.Sidebar.6"
            },
            { // 7
                element: '.fa-th-list',
                intro: "TOUR.Sidebar.7"
            },
            { // 8
                element: '.item>.fa-music',
                intro: "TOUR.Sidebar.8"
            },
            { // 9
                element: '.fa-atlas',
                intro: "TOUR.Sidebar.9"
            },
            { // 10
                element: '.fa-cogs',
                intro: "TOUR.Sidebar.10"
            },
            { // 11
                element: '.collapse',
                intro: "TOUR.Sidebar.11"
            },
        ];
    }
}
