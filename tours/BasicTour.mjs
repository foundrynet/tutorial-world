import BaseTour from "./BaseTour.mjs";

export default class BasicTour extends BaseTour {

    _onStepChange(currentStep, targetElement) {
        if (currentStep == 7) {
            document.querySelector('.item[data-tab="settings"]').click();
        }
    }

    /* -------------------------------------------- */

    _configureTour() {
        super._configureTour();

        this.tour.onexit(function() {
            game.settings.set("tutorial", "tourCompleted", true);
        });
        this.tour.oncomplete(function() {
            game.settings.set("tutorial", "tourCompleted", true);
        });
    }

    /* -------------------------------------------- */

    /**
     *
     * @return {TourStep[]}
     * @private
     * @override
     */
    _getTourSteps() {
        return [
            { // 0
                intro: "TOUR.Basic.0"
            },
            { // 1
                element: '#players',
                intro: "TOUR.Basic.1"
            },
            { // 2
                element: '#controls',
                intro: "TOUR.Basic.2"
            },
            { // 3
                element: '#navigation',
                intro: "TOUR.Basic.3"
            },
            { // 4
                element: '#sidebar-tabs',
                intro: "TOUR.Basic.4"
            },
            { // 5
                element: '#macro-list',
                intro: "TOUR.Basic.5",
                position: "top"
            },
            { // 6
                element: '.macro[data-slot="1"]',
                intro: "TOUR.Basic.6"
            },
            { // 7
                element: 'button[data-action="resettours"]',
                intro: "TOUR.Basic.7"
            },
        ];
    }
}
