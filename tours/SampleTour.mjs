import BaseTour from "./BaseTour.mjs";

export default class SampleTour extends BaseTour {

    _onStepChange(currentStep, targetElement) {
        if (currentStep == 0) {
            document.querySelector('#logo').click();
        }
    }

    /* -------------------------------------------- */

    /**
     *
     * @return {TourStep[]}
     * @private
     * @override
     */
    _getTourSteps() {
        return [
            { // 0
                element: "#logo",
                intro: "TOUR.Sample.0"
            },
        ];
    }
}
