import BasicTour from "./tours/BasicTour.mjs";
import SidebarTour from "./tours/SidebarTour.mjs";
import BaseTour from "./tours/BaseTour.mjs";
import SampleTour from "./tours/SampleTour.mjs";

Hooks.once('canvasReady', async () => {
    await BaseTour.delay(500);

    window.game.sampleTour = new SampleTour();
    window.game.basicTour = new BasicTour();
    window.game.sidebarTour = new SidebarTour();

    if (!game.settings.get("tutorial", "tourCompleted")) {
        window.game.basicTour.start();
    }
});

Hooks.on("ready", () => {

    game.settings.register("tutorial", "tourCompleted", {
        name: "Basic Tour Completed",
        scope: 'client',
        config: false,
        type: Boolean,
        default: false
    });
});

Hooks.on("renderSettings", (app, html) => {

    // Add button to Reset Tours
    let players = html.find("button[data-action='players']");
    $(`<button data-action="resettours"><i class="fas fa-hiking" aria-hidden="true"></i>Reset Tour</button>`).insertAfter(players);
    html.find('button[data-action="resettours"]').click(ev => {
        game.settings.set("tutorial", "tourCompleted", false);
        location.reload();
    });

});
