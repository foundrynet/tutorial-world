# Tutorial World

![](https://img.shields.io/badge/MinimumCoreVersion-9.221-red)
![](https://img.shields.io/badge/CompatibleCoreVersion-9-green)

A Foundry-ready world with a set of Tours provided by https://introjs.com/ in order to teach Users about the interface and features of Foundry.

![](./tour_screenshot.png)

## Tours Included

1. Basic - Shows the general high level UI elements such as the sidebar, and how to kick off other Tours
2. Sidebar - Describes each Sidebar tab and what it does, and shows how to collapse / expand


## Contributing Tours

### Step 1 - Create the files

1. Duplicate `tours/SampleTour.mjs` - this file controls the settings of your Tour
2. Add at least one new entry in the `languages/en.json` and other localization files for your Tour - I recommend the convention `TOUR.<TourName>.<StepIndex>`, such as `TOUR.Sample.0`. Separate your Tour from others with two newlines.
3. Import your new Tour in `tutorial.js` - at the top of the file, add an import such as `import SampleTour from "./tours/SampleTour.mjs";`
4. Initialize your Tour in `tutorial.js` - in the `Hooks.once('canvasReady')` method, add a line such as `window.game.sampleTour = new SampleTour();`. This will make your Tour avialable for use.
5. Add a macro to the Hotbar - This should be a `script` macro that executes your Tour, such as `window.game.sidebarTour.start();`

### Step 2 - Setup your steps

The minimal configuration for your Tour is to add a `_getTourSteps()` method with at least one step.

A Step consists of the following data:
```
intro - REQUIRED. A string that gets passed to localization, such as "TOUR.Sample.0".
element - OPTIONAL. A string that represents a CSS lookup. If correctly defined, the Tooltip will point at that element. Else, it will show up in the middle of the screen.
position - OPTIONAL. A string that tells the tooltip what side to show up on. By default it will prefer the right. Valid options are 'right', 'left', 'top', and 'bottom'
```

The minimal data example is
```js
_getTourSteps() {
        return [
            { // Step 0
                intro: "TOUR.Sample.0"
            },
        ];
    }
```

The full data example is
```js
_getTourSteps() {
        return [
            { // Step 0
                intro: "TOUR.Sample.0",
                element: "#logo",
                position: 'bottom'
            },
        ];
    }
```

### Step 3 - [OPTIONAL] Advanced configuration

#### _onStepChange

This method allows you to execute arbritary JS before a Step executes. This is useful for manipulating the data and UI state of Foundry before the Tooltip displays.

For example, this method clicks the Settings tab just before Step 7 is shown, ensuring the element we want to highligh is visible:

```js
    _onStepChange(currentStep, targetElement) {
        if (currentStep == 7) {
            document.querySelector('.item[data-tab="settings"]').click();
        }
    }
```

#### _configureTour

Allows for full access to the `IntroJS` tour API. See docs here: https://introjs.com/docs/intro/api

For example, this method sets a flag on complete / exit of the Tour so that we can track if it was completed:

```js
    _configureTour() {
        super._configureTour();

        this.tour.onexit(function() {
            game.settings.set("tutorial", "tourCompleted", true);
        });
        this.tour.oncomplete(function() {
            game.settings.set("tutorial", "tourCompleted", true);
        });
    }
```

#### Step 4 - Merge Request

Submit a Merge Request to this Repo, and we will try to review it in a timely manner. Please include a short description of what your Tour's theme is, along with a screenshot or gif of your Tour in action.

See a sample MR along with all the file changes you should have here: https://gitlab.com/foundrynet/tutorial-world/-/merge_requests/1
